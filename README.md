# newsleak

#docker installation

Install docker.
```
sudo apt update
```
```
sudo apt install apt-transport-https ca-certificates curl software-properties-common
```
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```
```
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
```
```
sudo apt update
```
```
apt-cache policy docker-ce
```
```
sudo apt install docker-ce
```
```
sudo systemctl status docker
```
#docker compose installation
```
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

docker-compose --version
```
#postgress installation
```
sudo apt update

```
```
sudo apt install postgresql postgresql-contrib
```
```
sudo -u postgres createuser newsleak
sudo -u postgres createdb newsleak
```
```
sudo -u postgres psql
psql=# alter user newsleak with encrypted password 'newsleak';
psql=# grant all privileges on database newsleak to newsleak ;
\q ## exit the psql shell
```

## configure psql using docker
```
sudo docker login
username:8837800242
password:123456789
sudo docker run --name postgresql-container -p 5432:5432 -e POSTGRES_PASSWORD=newsleak -d postgress
sudo docker ps -a ## verify new container
New container is running http://hubextractor.ciip-cis.co:5432 
```

#node js and npm installation
```
sudo apt install nodejs

nodejs -v

sudo apt install npm
```

# Install ngnix with lets encrypt

```
apt -y install software-properties-common
apt -y install nginx
add-apt-repository --yes ppa:certbot/certbot
apt update
apt -y install certbot
certbot certonly --webroot -w /var/www/html -d hubextractor.ciip-cis.co 
```
# After installing you recive save this notepad


```
- Congratulations! Your certificate and chain have been saved at:
/etc/letsencrypt/live/hubextractor.ciip-cis.co/fullchain.pem
Your key file has been saved at: /etc/letsencrypt/live/hubextractor.ciip-cis.co/privkey.pem

```

## create a cron job for auto-renwing certifcate on root if cron file is not created on root or error is showing no cron tab for root

```
crontab -e
-then press 2 it will create cron file  
enter tis command in file
36 2 * * * /usr/bin/certbot renew --post-hook "systemctl reload nginx"

#( Note:- To edit Nano file Please Press Ctrl+O then Press Ctrl+M after this Press Ctrl+X )
it will  install cron job 


```

## Create a new Nginx server block configuration of the reverse proxy for hubextractor

```
nano /etc/nginx/sites-available/hubextractor


#( Note:- To edit Nano file Please Press Ctrl+O then Press Ctrl+M after this Press Ctrl+X )

```
# Then Add this code in the file

```
server {
	listen 443;
	server_name hubextractor.ciip-cis.co;
	ssl_certificate
	/etc/letsencrypt/live/hubextractor.ciip-cis.co/fullchain.pem;
	ssl_certificate_key
	/etc/letsencrypt/live/hubextractor.ciip-cis.co/privkey.pem;
	ssl on;
	ssl_session_cache builtin:1000 shared:SSL:10m;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
	ssl_ciphers HIGH:!aNULL:!eNULL:!EXPORT:!CAMELLIA:!DES:!MD5:!PSK:!RC4;
	ssl_prefer_server_ciphers on;
	gzip on;
	gzip_http_version 1.1;
	gzip_vary on;
	gzip_comp_level 6;
	gzip_proxied any;
	gzip_types text/plain text/html text/css application/json application/javascript
	application/x-javascript text/javascript text/xml application/xml application/rss+xml
	application/atom+xml application/rdf+xml;
	gzip_buffers 16 8k;
	gzip_disable “MSIE [1-6].(?!.*SV1)”;
	access_log /var/log/nginx/hubextractor.access.log;
	location / {
		proxy_pass
		http://159.203.65.78:9000;
		proxy_set_header host $host;
		proxy_http_version 1.1;
		proxy_set_header upgrade $http_upgrade;
		proxy_set_header connection "upgrade";
	}

}
- Activate the configuration file by running.

```
ln -s /etc/nginx/sites-available/hubextractor /etc/nginx/sites-enabled/hubextractor

```

- You can verify if the configuration file is error free by running nginx -t.

```
root@aliyun:~# nginx -t

```

- Restart the Nginx web server so that the change in configuration can be applied

```
systemctl restart nginx

```
#( Note:- To edit Nano file Please Press Ctrl+O then Press Ctrl+M after this Press Ctrl+X )
```

##Adjusting the Firewall Rules
## check status of firewall

```
sudo ufw status

```
## Status is not active then enable it on your server it is off you need to enable it is already install in our server

```
sudo ufw enable
sudo ufw status

```
## Enable some firewal extensions

```
sudo ufw allow http
sudo ufw allow https
sudo ufw allow OpenSSH
```
## Infomation for another server if ufw not install you need to type
```
sudo apt-get install ufw
sudo ufw status
sudo ufw allow http
sudo ufw allow https
sudo ufw allow OpenSSH
```
#newsleak docker installation

```
git clone https://github.com/uhh-lt/newsleak-docker.git
cd newsleak-docker
``` 
```
nano postgres.env ## Add database name,user,password that you create at the time of postgress installation like i set this
POSTGRES_PASSWORD=felipe@123
POSTGRES_USER=felipe 
POSTGRES_DB=felipe

you can add what you set at installation time
```
```
nano docker-compose.yml
```

Start containers.

```
docker-compose up -d
```
### Minimum setup

Basic security settings for the newsleak UI can be setup in the file `volumes/ui/conf/application.production.conf`:

1. Set custom user credentials for basic user authentication
2. Set a custom secret key for the Play application framework
3. Set custom passwords for the newsleak postgres database (`postgres.env`, `volumes/ui/conf/newsleak.properties`, `volumes/ui/conf/application.production.conf`)
4. Put everything behind a SSL proxy which only forwards to http://localhost:9000, if you want to enable access via the internet 


# Hoover installation

1. Increase `vm.max_map_count` to at least 262144, to make elasticsearch happy
   ```
   sudo nano /etc/sysctl.conf 
   vm.max_map_count=262144  #add these lines
   sysctl -p
   cat /proc/sys/vm/max_map_count
   ```

2. Install docker:

    ```
    apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common python3.6     python3-pip
    pip3 install -r requirements.txt
    
    ```

3. Clone the repo and set up folders:

    ```shell
    git clone https://github.com/hoover/docker-setup /opt/hoover
    cd /opt/hoover
    mkdir -p volumes volumes/metrics volumes/metrics/users volumes/search-es-snapshots volumes/search-es/data collections
    chmod 777 volumes/search-es-snapshots volumes/search-es/data
    ```

4. Spin up the docker containers, run migrations, create amdin user:

    ```shell
    docker-compose run --rm search ./manage.py migrate
    docker-compose run --rm search ./manage.py createsuperuser
    docker-compose run --rm ui npm run build
    docker-compose run --rm search ./manage.py collectstatic --noinput
    docker-compose up -d
    ```

5. Import the test dataset:

    ```shell
    git clone https://github.com/hoover/testdata collections/testdata
    ./createcollection -c testdata
    ```

## After Data import or hoover installation


```
nano volumes/ui/conf/newsleak.properties
```
## Install newsleak 
```
git clone https://github.com/uhh-lt/newsleak.git

cd newsleak-docker
docker-compose down
docker-compose -f docker-compose.dev.yml up -d
nano conf/application.conf
# Available ES collections (provide db.* configuration for each collection below)
es.indices =  [newsleakdev,newsleak2,newsleak3,newsleak4,newsleak5]
# ES connection
es.clustername = "elasticsearch"
es.address = "hubextractor.ciip-cis.co"
es.port = 19300

# Determine the default dataset for the application
es.index.default = "newsleakdev"

# collection 1
db.newsleakdev.driver=org.postgresql.Driver
db.newsleakdev.url="jdbc:postgresql://hubextractor.ciip-cis.co:15432/newsleak"
db.newsleakdev.username="user"
db.newsleakdev.password="password"
es.newsleakdev.excludeTypes = [Link,Filename,Path,Content-type,SUBJECT,HEADER,Subject,Timezone,sender.id,Recipients.id,Recipients.order]
npm install
sbt run

```
Open the UI application in your browser

```
https://hubextractor.ciip-cis.co:9000/
```
# To restart application.production.conf
```
docker-compose restart
```
# To update newsleak
```
cd preprocessing
mvn clean package assembly:single
cd ..
sbt dist
unzip target/universal/newsleak-ui.zip -d target/universal/
docker build -t uhhlt/newsleak:v1.0 .
docker-compose up -d
docker exec -it newsleak sh -c "cd /opt/newsleak && java -Xmx10g -jar preprocessing.jar -c /etc/settings/conf/newsleak.properties"


#For spanish language

nano conf/application.conf

```
play.i18n.langs= ["es"]

```




